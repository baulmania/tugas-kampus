<?php
// Check existence of id parameter before processing further
if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
    // Include config file
    require_once "connection.php";

    // Prepare a select statement
    $sql = "SELECT * FROM visitors WHERE id = ?";

    if ($stmt = mysqli_prepare($link, $sql)) {
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);

        // Set parameters
        $param_id = trim($_GET["id"]);

        // Attempt to execute the prepared statement
        if (mysqli_stmt_execute($stmt)) {
            $result = mysqli_stmt_get_result($stmt);

            if (mysqli_num_rows($result) == 1) {
                /* Fetch result row as an associative array. Since the result set
                contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

                // Retrieve individual field value
                $name = $row["name"];
                $sql = ("select * from vehicle where id = " . $row['vehicle_id']);
                $data = mysqli_query($link, $sql);
                $dataType = mysqli_fetch_array($data, MYSQLI_ASSOC);

                $to_time = strtotime($row['created_at']);
                $from_time = strtotime($row['updated_at']);
                //get biaya parkir
                $perJam = 2000;
                $nextJam = 1000;
                $round = (round(abs($from_time - $to_time) / 60));
                $additional = intval($round / 60);
                $bayar = $perJam + $additional * $nextJam;
            } else {
                // URL doesn't contain valid id parameter. Redirect to error page
                header("location: error.php");
                exit();
            }
        } else {
            echo "Oops! Something went wrong. Please try again later.";
        }
    }

    // Close statement
    mysqli_stmt_close($stmt);

    // Close connection
    mysqli_close($link);
} else {
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper {
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="mt-5 mb-3">View Record</h1>
                    <div class="form-group">
                        <label>Name</label>
                        <p><b><?php echo $row["name"]; ?></b></p>
                    </div>
                    <div class="form-group">
                        <label>Type</label>
                        <p><b><?php echo $dataType["type"]; ?></b></p>
                    </div>
                    <div class="form-group">
                        <label>Jam Masuk</label>
                        <p><b><?php echo $row["created_at"]; ?></b></p>
                    </div>
                    <div class="form-group">
                        <label>Jam Keluar</label>
                        <p><b><?php echo $row["updated_at"]; ?></b></p>
                    </div>
                    <p><a href="index.php" class="btn btn-primary">Back</a></p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>