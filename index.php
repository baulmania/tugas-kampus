<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        .wrapper {
            width: 1300px;
            margin: 0 auto;
        }

        table tr td:last-child {
            width: 150px;
        }

        .table-fixed tbody {
            height: 300px;
            overflow-y: auto;
            width: 100%;
        }

        .table-responsive {
            max-height: 500px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
        setTimeout(function() {
            location = ''
        }, 60000)
    </script>
</head>

<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="mt-5 mb-1 clearfix">
                        <h2 class="pull-left">Parkir XYZ</h2>
                        <a href="create.php" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New Visitor</a>

                    </div>
                    <div>
                        <h4 class="pull-left">1 jam pertama 2000, 1 jam selanjutnya 1000</h4><br>
                    </div>
                    <div class="table-responsive">
                        <?php
                        // Include config file
                        require_once "connection.php";

                        // Attempt select query execution
                        $sql = "SELECT * FROM visitors";
                        if ($result = mysqli_query($link, $sql)) {
                            if (mysqli_num_rows($result) > 0) {
                                echo '<table class="table table-bordered table-striped table-fixed">';
                                echo "<thead>";
                                echo "<tr>";
                                echo "<th scope='col' class='col-1'>No</th>";
                                echo "<th scope='col' class='col-2'>Plat Nomor</th>";
                                echo "<th scope='col' class='col-1'>type</th>";
                                echo "<th scope='col' class='col-2'>Jam Masuk</th>";
                                echo "<th scope='col' class='col-2'>Jam Keluar</th>";
                                echo "<th scope='col' class='col-2'>Lama Parkir</th>";
                                echo "<th scope='col' class='col-1'>Jumlah Bayar</th>";
                                echo "<th scope='col' class='col-2'>Action</th>";
                                echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                                while ($row = mysqli_fetch_array($result)) {
                                    //get lama parkir
                                    $tz = 'Asia/Jakarta';
                                    $dt = new DateTime("now", new DateTimeZone($tz));
                                    $DateTimeNow =  strtotime($dt->format('Y-m-d H:i:s'));
                                    $to_time = strtotime($row['created_at']);
                                    $from_time = strtotime($row['updated_at']);
                                    //get biaya parkir
                                    $perJam = 2000;
                                    $nextJam = 1000;
                                    $round = (round(abs($from_time - $to_time) / 60));
                                    $additional = intval($round / 60);
                                    $bayar = $perJam + $additional * $nextJam;


                                    echo "<tr>";
                                    echo "<td>" . $row['id'] . "</td>";
                                    echo "<td>" . $row['name'] . "</td>";
                                    $sql = ("select * from vehicle where id = " . $row['vehicle_id']);
                                    $data = mysqli_query($link, $sql);
                                    echo "<td>" . $data->fetch_object()->type . "</td>";
                                    echo "<td>" . $row['created_at'] . "</td>";
                                    echo "<td>" . $row['updated_at'] . "</td>";
                                    if ($row['updated_at'] == null) {
                                        echo "<td>" . round(abs($DateTimeNow - $to_time) / 60) . " minute</td>";
                                    } else {
                                        echo "<td>" . round(abs($from_time - $to_time) / 60) . " minute</td>";
                                    }
                                    if ($row['updated_at'] == null) {
                                        echo "<td> Blm Keluar</td>";
                                    } else {
                                        echo "<td>Rp." . $bayar . " </td>";
                                    }
                                    echo "<td>";
                                    echo '<a href="read.php?id=' . $row['id'] . '" class="mr-3" title="View Record" data-toggle="tooltip"><span class="fa fa-eye"></span></a>';
                                    echo '<a href="update.php?id=' . $row['id'] . '" class="mr-3" title="Update Record" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>';
                                    echo '<a href="delete.php?id=' . $row['id'] . '" title="Delete Record" data-toggle="tooltip"><span class="fa fa-trash"></span></a>';
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                echo "</tbody>";
                                echo "</table>";
                                // Free result set
                                mysqli_free_result($result);
                            } else {
                                echo '<div class="alert alert-danger mt-5"><em>No records were found.</em></div>';
                            }
                        } else {
                            echo "Oops! Something went wrong. Please try again later.";
                        }

                        // Close connection
                        mysqli_close($link);
                        ?>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>