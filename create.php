<?php
// Include config file
require_once "connection.php";
$tz = 'Asia/Jakarta';
$dt = new DateTime("now", new DateTimeZone($tz));
// Define variables and initialize with empty values
$name = $type = "";
$name_err = $type_err =  "";

// get type kendaraan
$sql = "SELECT * FROM vehicle";
$result = mysqli_query($link, $sql);
// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Validate name
    $input_name = trim($_POST["name"]);
    if (empty($input_name)) {
        $name_err = "Please enter a name.";
    } else {
        $name = strval($input_name);
    }
    // Validate type
    $input_type = trim($_POST["type"]);
    if (empty($input_type)) {
        $type_err = "Please enter an type.";
    } else {
        $type = strval($input_type);
    }

    // Check input errors before inserting in database
    if (empty($name_err) && empty($type_err)) {
        $created_at =  $dt->format('Y-m-d H:i:s');
        // Prepare an insert statement
        $sql = "INSERT INTO visitors (name, vehicle_id, created_at) VALUES (?, ?, ?)";

        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters

            mysqli_stmt_bind_param($stmt, "sss", $param_name, $param_type, $created_at);

            // Set parameters

            $param_name = strval($name);
            $param_type = $type;
            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper {
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-5">Create Record</h2>
                    <p>Please fill this form and submit to add employee record to the database.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                        <div class="form-group">
                            <label>Plat Nomor</label>
                            <input type="text" name="name" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">
                            <span class="invalid-feedback"><?php echo $name_err; ?></span>
                        </div>

                        <div class="form-group">
                            <label>type</label>
                            <div class="input-group mb-3">
                                <select class="custom-select" id="inputGroupSelect02" name="type">
                                    <?php
                                    while ($row = mysqli_fetch_array($result)) {
                                    ?>
                                        <option value=<?php echo $row['id'] ?> <?php echo (!empty($type_err)) ? 'is-invalid' : ''; ?>><?php echo $row['type'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <span class="invalid-feedback"><?php echo $type_err; ?></span>
                                <div class="input-group-append">
                                    <label class="input-group-text" for="inputGroupSelect02">Type Kendaraan</label>
                                </div>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>